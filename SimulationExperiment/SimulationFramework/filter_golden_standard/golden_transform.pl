#!/usr/bin/perl -w
use strict;

if($#ARGV!=2)
{
    print "script usage: script dict input output\n"
}

my $dict=$ARGV[0];
my $input=$ARGV[1];
my $output=$ARGV[2];

my @arrayone;
my @arraytwo;
my @array;

my ($i,$j);

my $line;

my $dicthash={};
open(DICT, $dict);
while ($line=<DICT>)
{
    @array=split(' ', $line);
    if (exists $dicthash->{$array[1]})
    {
        next;
    }
    else
    {
        $dicthash->{$array[1]}=$array[0];
    }
}
close(DICT);

open(IN, $input);
open(OUT, '>'.$output);

while ($line=<IN>)
{
    @array=split(' ', $line);
    if (exists $dicthash->{$array[0]} && exists $dicthash->{$array[1]})
    {
        print OUT $dicthash->{$array[0]}."\t".$dicthash->{$array[1]}."\n";
    }
}

close(IN);
close(OUT);
