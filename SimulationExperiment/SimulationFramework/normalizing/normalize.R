load("filter_expression.RData")

n <- dim(expression)[1]
T <- dim(expression)[2]

expression <- t(scale(t(expression)))
save(expression, file = 'filter_norm_expression.RData')
