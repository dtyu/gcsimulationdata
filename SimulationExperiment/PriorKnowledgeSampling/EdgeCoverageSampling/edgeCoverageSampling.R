# Random samping the prior knowledge using R

# sampling rate
threshold <- 0.5

# Control the random seed.
set.seed(1)

# Read the prior knowledge from the file.
prior <- read.table("filter_prior_knowledge", header=F, sep="\t")

filename <- paste("prior_knowledge_", threshold, sep="")
sink(filename, append=F)
for (i in 1:dim(prior)[1]) {
    if (runif(1, 0.0, 1.0) <= threshold) {
        cat(sprintf("%d\t%d\n", prior[i, 1], prior[i, 2]))
    }
}
sink()
