# Random samping the prior knowledge using R

# Control the random seed.
set.seed(1)

threshold <- 0.8

library(igraph)

# Read the prior knowledge from the file.
prior <- read.table("filter_prior_knowledge", header=F, sep="\t")

# Treating all the edges directed.
# Generate a graph.
g <- graph.data.frame(prior, directed=T)
nodecount <- vcount(g)

# Start BFS
# Random sample a 
st <- sample(1:nodecount, 1)

# Generate a connected set of nodes through BFS
x <- bfs(g, as.character(st), neimode = "all")$order
seq <- as.numeric(x$name)

# Generate hashset.
library(hash)
set <- hash(seq[1:as.integer(threshold * nodecount)], T)

# Only if both vertex is in the set, we include the edge in the result
filename <- paste("coverage_sampling_", threshold, sep="")
sink(filename, append=F)
for (i in 1:dim(prior)[1]) {
    if (has.key(as.character(prior[i,1]), set) & has.key(as.character(prior[i,2]), set)) {
        cat(sprintf("%d\t%d\n", prior[i, 1], prior[i, 2]))
    }
}
sink()
